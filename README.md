# Feed assignment (candidate)

Dear candidate,

Thank you for your interest in working with us at Telenor Digital Asia and taking the time to help us get familiar with your profressional skills.

This test is your opportunity to show of your technical excellence. It is an important part in
showing who you are and what you stand for.

In many aspects this test is more important than the resume you sent us. It tells us about the
choices you make as a developer, and how you solve problems presented to you.

## Guidelines

- Please use good programming practices when solving the problem.

- Please provide automated unit tests.

- Please submit your code as a link to a Git/Mercurial repository somewhere or a zip/tar.gz file with all the commit history.

- Please submit a solution that has the quality of your real work standards.

## Assignment description

- Nodejs (required)

## Task

1. Periodically read feed from https://www.reddit.com/domain/youtube.com/.rss and store it in a database (of your choice)

2. Create RESTful web service to provide those data.  An example client is provided that you can use to consume the API, you can run it with `npm run frontend`

3. Create endpoint to return top 10 of subreddit name and number of times that subreddit appear the most in decending order

With best regards,

Telenor Digital Asia


## Setup Guide
This program is using `Node.js`  
Please make sure you have `Node.js version 8` or higher  
[Click here to see more details](https://nodejs.org/en/)

This project requires Mongo DB setup

## ENV Var
- MONGO_URL : mongo db connection string e.g. mongodb://localhost:32768/feed


## How To Run
For server side, will start at default port 3001
```
npm run server
```

For frontend side, will start at default port 3000
```
npm run frontend
```

## Testing
This project main logic RSS to JSON is using lib called `feedme`

__Current project status__

<p><a href="http://travis-ci.org/fent/feedme.js" rel="nofollow"><img src="https://secure.travis-ci.org/fent/feedme.js.svg" alt="Build Status"></a>
<a href="https://david-dm.org/fent/feedme.js" rel="nofollow"><img src="https://david-dm.org/fent/feedme.js.svg" alt="Dependency Status"></a>
<a href="https://codecov.io/gh/fent/feedme.js" rel="nofollow"><img src="https://codecov.io/gh/fent/feedme.js/branch/master/graph/badge.svg" alt="codecov"></a></p>

You can check it's code coverage [Here](https://codecov.io/gh/fent/feedme.js)

## API Doc

### GET /subreddit
__Example response__
```json
{
    subreddits: [{
            "_id": "t3_9q55vp",
            "title": "Welcome TSM Zikz",
            "content": "<table> <tr><td> <a href=\"https://www.reddit.com/r/leagueoflegends/comments/9q55vp/welcome_tsm_zikz/\"> <img src=\"https://b.thumbs.redditmedia.com/FAruS2HkPwjkZjkoatO-j4uJ1J1Bo_M22U2vK9pvO5E.jpg\" alt=\"Welcome TSM Zikz\" title=\"Welcome TSM Zikz\" /> </a> </td><td> &#32; submitted by &#32; <a href=\"https://www.reddit.com/user/corylulu\"> /u/corylulu </a> &#32; to &#32; <a href=\"https://www.reddit.com/r/leagueoflegends/\"> r/leagueoflegends </a> <br/> <span><a href=\"https://www.youtube.com/watch?v=lzBh-LApAvI\">[link]</a></span> &#32; <span><a href=\"https://www.reddit.com/r/leagueoflegends/comments/9q55vp/welcome_tsm_zikz/\">[comments]</a></span> </td></tr></table>",
            "category": "leagueoflegends",
            "link": "https://www.reddit.com/r/leagueoflegends/comments/9q55vp/welcome_tsm_zikz/",
            "createdAt": "2018-10-21T19:51:21.453Z",
            "updatedAt": "2018-10-21T20:20:37.319Z"
        },
        ...]
}
```

### GET /rank
__Example response__
```json
{
    rank : [{
            "_id": "The_Donald",
            "count": 44,
            "createdAt": "2018-10-21T19:56:10.278Z",
            "updatedAt": "2018-10-21T20:20:37.358Z",
            "name": "The_Donald"
        },
        {
            "_id": "videos",
            "count": 33,
            "createdAt": "2018-10-21T19:56:10.273Z",
            "updatedAt": "2018-10-21T20:20:37.358Z",
            "name": "videos"
        },
        {
            "_id": "Music",
            "count": 33,
            "createdAt": "2018-10-21T19:56:10.276Z",
            "updatedAt": "2018-10-21T20:20:37.352Z",
            "name": "Music"
        }]
}
```

