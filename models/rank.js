const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const RankSchema = new Schema(
  {
    _id: String,
    name: String,
    count: Number
  },
  { versionKey: false, timestamps: true }
);

const Rank = mongoose.model("rank", RankSchema);

module.exports = Rank;
