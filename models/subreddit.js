const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SubRedditSchema = new Schema(
  {
    _id: String,
    title: String,
    content: String,
    category: String,
    link: String
  },
  { versionKey: false, timestamps: true }
);

const SubReddit = mongoose.model("subreddit", SubRedditSchema);

module.exports = SubReddit;
