require("dotenv").config();

const express = require("express");

const feed = require("./src/feedCtrl");

const app = express();
const port = process.env.PORT || 3001;

if (process.env.NODE_ENV !== "test") {
  require("./src/database/mongoClient");
}

/**                 **
 *  YOUR CODE HERE   *
 *                  **/

app.get("/subreddit", async (req, res) => {
  try {
    const lists = await feed.getAllSubReddit();
    return res.json({ subreddits: lists });
  } catch (error) {
    return res.status(400).send(error.meesage);
  }
});

app.get("/rank", async (req, res) => {
  try {
    const lists = await feed.getRank();
    return res.json({ rank: lists });
  } catch (error) {
    return res.status(400).send(error.meesage);
  }
});

app.listen(port, () => {
  console.log(`API app listening on port ${port}!`);
  feed.initFeed();
});
