const FeedMe = require("feedme");
const axios = require("axios");

const SubReddit = require("../models/subreddit");
const Rank = require("../models/rank");

function readFeed() {
  return axios({
    method: "get",
    url: "https://www.reddit.com/domain/youtube.com/.rss",
    responseType: "stream"
  }).then(
    res =>
      new Promise((reslove, reject) => {
        if (res.status != 200) {
          console.error(new Error(`status code ${res.status}`));
          reject(`status code ${res.status}`);
        }

        const parser = new FeedMe(true);

        res.data.pipe(parser);

        parser.on("end", () => {
          const JsonData = parser.done();
          reslove(JsonData);
        });
      })
  );
}

async function writeToDatabase(lists) {
  const writeToDBPromise = lists.items.map(async data => {
    console.log(`Writing ${data.title} to database..`);
    await SubReddit.updateOne(
      { _id: data.id },
      {
        title: data.title,
        content: data.content,
        category: data.category.term,
        link: data.link.href
      },
      {
        upsert: true,
        setDefaultsOnInsert: true
      }
    );

    return Rank.updateOne(
      { _id: data.category.term },
      { name: data.category.term, $inc: { count: 1 } },
      {
        upsert: true,
        setDefaultsOnInsert: true
      }
    );
  });
  return Promise.all(writeToDBPromise);
}

function getAllSubReddit() {
  return SubReddit.find({});
}

async function getRank() {
  return Rank.find({})
    .sort({ count: -1 })
    .limit(10);
}

module.exports = {
  initFeed: () => readFeed().then(writeToDatabase),
  getAllSubReddit,
  getRank
};
